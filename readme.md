Welcome to COMP9900 SkillBackpack by WannaSmile by-
Nihit Vyas
Aipeng Chan
Cong Hai
Lean Oh

The database definitions are in the file - `skillbackpack/portfolio/models.py`
Project views are located in - `skillbackpack/portfolio/views.py`

If you are running the zipped version, To run the application follow the steps:

  1) Install project dependencies by typing - 
        `pip3 install -r requirements.txt`


  2) Navigate to the directory `skillbackpack`. In this directory type:
        `python3 manage.py makemigrations`
        `python3 manage.py migrate`
     This performs database migrations for the project.

  3) To run the webserver type:
        `python3 manage.py runserver`
     This starts the Django server on the port 8000

  4) To view the web interface open your web browser and go to `localhost:8000`
     You can now see the homepage of our application.
     For user instructions please refer to the User Manual in the submitted report.

  5) To access the admin interface of the application, you need to create a superuser:
      Inside the skillbackpack directory type:
        `python3 manage.py createsuperuser`
      type in a username, password and email.
      Restart the server - `python3 manage.py runserver`

  6) Navigate to `localhost:8000/admin` in the web browser and login with the superuser credentials created in the previous step. On the admin interface you will be able to see the various database tables segmented according to the Django app they belong to.


If you have cloned from the Bitbucket repo follow -
    To run the application install the project dependencies listed in requirements.txt file and Python3.6.

    `pip install -r requirements.txt`

    Navigate to the directory `skillbackpack`. To start the server type:

    `python manage.py runserver`

    To view the application go to your browser:
    `localhost:8000`
