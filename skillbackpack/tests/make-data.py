#!/usr/bin/env python3
#
# Usage:
#
# from 'tests' dir, run:
#
#    $ ./make-users.py
#
# It will insert new user entry into current project's database.
#
# To login:
#
#   Use one of the usernames generated from the stdout by the script,
#   then login (as employer or as student), using the password as
#   noted in the comment beside the "password_hash" variable value
#   below
# 
# To set another password manually:
#   - generate a new user with the desired password
#   - run "python3 manage.py dumpdata auth.User --indent 4 >
#       fixtures/orig-users.json"
#   - open the orig-users.json to see the password hash of the
#       manually-created user
#   - use that hash in this code (might be necessary to remove all
#       users added to the database by this code previously to avoid
#       primary key conflict)

# TODO: look up user meaningful name from dictionary 'dict/'

nb_of_newusers= 10

# primary key used for new user (will be incremented for each new
#   user):
start_prime_key = 500

# --- ORDER ---
dir_fixtures = "tests/fixtures"
newuser_fname = dir_fixtures + "/new-users.json"
# ---

password_hash = 'pbkdf2_sha256$100000$7JFxOqfHe66M$EgeM5gnP5/TcFccIoyO8k9MbAprXgiBOK865huZYE28=' # abc123./


def make_new_users(are_employers=False):
    print("New user names created:")
    newuser_file = open(newuser_fname,"w")
    newuser_file.write('[')
    for prime_key in range(start_prime_key,start_prime_key + nb_of_newusers):
        if (prime_key != start_prime_key):
                newuser_file.write(',')
        newuser_file.write('{')
        newuser_file.write('"model": "auth.user",')
        newuser_file.write('"pk": {},'.format(prime_key))

        username= "t" + str(prime_key)

        newuser_file.write('"fields": {')
        newuser_file.write('"password":')
        newuser_file.write( '"{}",'.format(password_hash))
        newuser_file.write('"last_login": "2018-10-02T22:51:12.347Z",')
        newuser_file.write('"is_superuser": false,')
        newuser_file.write('"username": "{}",'.format(username))
        newuser_file.write('"first_name": "{}",'.format(username))
        newuser_file.write('"last_name": "{}",'.format(username))
        newuser_file.write('"email": "{}@gmail.com",'.format(username))
        newuser_file.write('"is_staff": false,')
        newuser_file.write('"is_active": true,')
        newuser_file.write('"date_joined": "2018-10-02T22:51:11.877Z",')
        newuser_file.write('"groups": [],')
        newuser_file.write('"user_permissions": []')
        newuser_file.write('}')
        newuser_file.write('}')
        #
        # adding entry to admin.logentry:
        newuser_file.write(',')
        newuser_file.write('{')
        newuser_file.write('"model": "admin.logentry",')
        newuser_file.write('"pk": {},'.format(prime_key))
        newuser_file.write('"fields": {')
        newuser_file.write('"action_time": "2018-10-01T04:25:50.104Z",')
        newuser_file.write('"user": 12,')
        newuser_file.write('"content_type": 20,')
        newuser_file.write('"object_id": "24",') # this is id of organization
        newuser_file.write('"object_repr": "aws",')
        newuser_file.write('"action_flag": 1,')
        newuser_file.write('"change_message": "[{\\"added\\": {}}]"')
        newuser_file.write('}')
        newuser_file.write('}')
        if (are_employers):
        # now make that user an employer:
            newuser_file.write(',')
            newuser_file.write('{')
            newuser_file.write('"model": "user_management.employer",')
            newuser_file.write('"pk": {},'.format(prime_key))
            newuser_file.write('"fields": {')
            newuser_file.write('"username": {},'.format(prime_key))
            newuser_file.write('"description": "Description. Some description goes into this."')
            newuser_file.write('}')
            newuser_file.write('}')
        print(username)
    newuser_file.write(']')
    newuser_file.close()


from subprocess import call
def load_new_users():
    call(["python3", "manage.py", "loaddata", newuser_fname])


if __name__ == "__main__":
    import os
    os.chdir("..")
    if not os.path.exists("manage.py"):
        print("Error: please invoke this script in the 'tests' dir (where it can look up 'manage.py' in its parent dir)!")
        exit(1)
    if not os.path.exists(dir_fixtures):
        os.makedirs(dir_fixtures)
    make_new_users(are_employers=True)
    load_new_users()
