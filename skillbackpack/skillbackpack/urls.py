"""skillbackpack URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import login, logout
from django.conf.urls import url, include
from portfolio import views as portfolio_views
from user_management import views as user_views

urlpatterns = [
    url(r'^$', portfolio_views.index, name='index'),
    path('admin/', admin.site.urls),
    url(r'^portfolio/', include(('portfolio.urls', 'portfolio'), namespace='portfolio')),
    url(r'^portfolio/', include(('user_management.urls', 'user_management'), namespace='user_management')),
    url(r'^accounts/logout/$', logout, name='logout'),
    url(r'^accounts/login/$', user_views.signin, name='login'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
