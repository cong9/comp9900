from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class SkillRatings(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    keyword = models.CharField(max_length=150, default=None)
    rating = models.FloatField(default=1)

    class Meta:
        verbose_name_plural = "SkillRatings"

    def __str__(self):
        return str(self.keyword)
