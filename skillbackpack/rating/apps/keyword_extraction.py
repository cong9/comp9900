import string
# import spacy
# from multi_rake import Rake


def load_skill_table(table_name):
    with open(table_name) as file:
        skill_list = file.read().strip().split('\n')

    skill_table = {}
    for skill in skill_list:
        parts = skill.split(' ')
        if len(parts) > 1:
            skill_table[parts[0]] = parts[1:]
        else:
            skill_table[skill] = []
    return skill_table


def extract_matching(text, text_type=None):
    skills = {}

    translator_punc = str.maketrans(string.punctuation, ' ' * len(string.punctuation))  # replace all puctuations with space
    text = text.translate(translator_punc)

    skill_table = load_skill_table('rating/apps/skills.txt')  # read skills from text file

    text = text.strip().split(' ')
    text_len = len(text)
    for i, word in enumerate(text):
        word = word.lower()
        if word in skill_table.keys():
            if skill_table.get(word) is not None:
                if len(skill_table.get(word)) > 0:  # for phrases
                    if not ' '.join(text[i:i+1+len(skill_table.get(word))]) == ' '.join([word] + skill_table.get(word)):
                        continue
                    else:
                        skill = ' '.join(text[i:i+1+len(skill_table.get(word))])
                else:
                    skill = word
                if skills.get(skill) is None:
                    skills[skill] = {'rate': 0, 'count': 0, 'last_index': -1}
                last_index = skills[skill]['last_index']
                skills[skill]['count'] += 1
                if last_index != -1:
                    skills[skill]['rate'] += (i - last_index - 1)/text_len
                else:
                    skills[skill]['rate'] += 1
                skills[skill]['last_index'] = i

    return skills


if __name__ == '__main__':
    # k = extract(test_text)
    # print(k[:3])
    pass
