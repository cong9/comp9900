from django.apps import AppConfig
import re
import requests
import json

# from rating.models import SkillRatings
from rating.apps.keyword_extraction import extract_matching
from rating.models import SkillRatings
# import portfolio.models as m
from portfolio.models import Skill, Project, Experience, Education

test_text = """
    flask machine_learning As a student application developer at UNSW, I work for the Educational Intelligence Services at the UNSW Office of Pro-Vice Chancellor Education, developing applications to provide service improvement for academic and administrative departments.
My job responsibilities include-
•	Collaboratively working in teams to develop innovative educational technology applications.
•	Full-stack development with React, Django django and Flask.
•	Integrating applications with web services using REST APIs.
•	Developing ETL pipelines for machine learning university data.
•	Liaising with clients and stakeholders regarding requirement specifications, flask project updates and delivery."
"""

test_2 = """
flask python python python python python python python python python python python python python flask python flask
"""

related_coff = 0.8

class RatingConfig(AppConfig):
    name = 'rating'


def save_keyword(user, keywords):
    for rank, keyword in enumerate(keywords):
        SkillRatings.objects.create(user=user, keyword=keyword, rating=rank+1)


def get_keyword(user):
    keywords = SkillRatings.objects.filter(user=user)
    return keywords


def get_github_link(text):
    pattern = re.complile('http(s)?://(www.)?github.com/\S/\S')
    print(re.search(pattern, test_text))


def github_rating(link):
    stat_link = re.sub(r'github.com', 'api.github.com/repos', link)
    stats = requests.get(stat_link).json()
    star = stats.get('stargazers_count')
    language = stats.get('language').lower()
    commits_link = stat_link + '/commits'
    commits = requests.get(commits_link)
    commit_counts = len(commits)
    return language, star, commit_counts


def get_github_link(text):
    pattern = re.compile(r'(http(s)?://)?(www.)?github.com/[0-9a-zA-Z\_\-]+/[0-9a-zA-Z\_\-]+')
    links = pattern.findall(test_text)
    return links


def get_skill_rating(texts):
    ratings = {}
    for text in texts:
        if text is None:
            continue
        text_rating = extract_matching(text)
        for skill, rates in text_rating.items():
            if ratings.get(skill) is None:
                ratings[skill] = 0
            ratings[skill] += rates['rate']
    return ratings


def update_skill_rating(user):
    # update a user's skill rating whenever one of these fields are updated
    skills = Skill.objects.filter(user=user).all()
    projects = Project.objects.filter(user=user).all()
    education = Education.objects.filter(user=user).all()
    experience = Experience.objects.filter(user=user).all()
    texts = []
    for skill in skills:
        texts.append(skill.description + skill.get_name_display())
    for project in projects:
        texts.append(project.description)
    for edu in education:
        texts.append(edu.description)
    for exp in experience:
        texts.append(exp.description)
    ratings = get_skill_rating(texts)
    SkillRatings.objects.filter(user=user).exclude(keyword__in=ratings.keys()).delete()
    for keyword, rating in ratings.items():
        skill_rating, created = \
            SkillRatings.objects.update_or_create(user=user, keyword=keyword, defaults={'rating': rating})


def load_relation(relation_file):
    import json
    with open(relation_file) as rel:
        return json.loads(rel.read())


def get_search(keyword):
    related = {1: [], 2: []}
    if skill_relations.get(keyword) is None:
        return {}
    related[1] = skill_relations[keyword]
    for r in related[1]:
        for k in skill_relations[r]:
            if k != keyword and k not in related[1] and k not in related[2]:
                related[2].append(k)
    ratings = SkillRatings.objects.filter(keyword__in=[keyword]+related[1]+related[2])  # get all skills with distance <= 2
    user_ratings = {}
    for rating in ratings:
        score = 0
        user_id = rating.user_id
        if rating.keyword == keyword:
            score += rating.rating
        elif rating.keyword in related[1]:
            score += rating.rating * related_coff
        elif rating.keyword in related[2]:
            score += rating.rating * (related_coff ** 2)
        if user_ratings.get(user_id) is None:
            user_ratings[user_id] = 0
        user_ratings[user_id] += score
    return user_ratings


skill_relations = load_relation("rating/apps/skills_graph.json")
if __name__ == '__main__':
    get_search("sklearn")
