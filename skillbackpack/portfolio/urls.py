from django.conf.urls import url
from .views import *

urlpatterns = [
    #url(r'^me/', detail_view, name='portfolio'),
    url(r'^accounts/profile/$', user_portfolio, name='account_redirect'),
    url(r'^accounts/profile/delete_summary/(?P<pk>\d+)/$', delete_summary, name="delete_summary"),
    url(r'^accounts/profile/education/$',add_education, name='education'),
    url(r'^accounts/profile/education/(?P<pk>\d+)/$',EducationUpdate.as_view(), name='education_update'),
    url(r'^accounts/profile/education/delete/(?P<pk>\d+)/$', delete_education, name='delete_education'),


    url(r'^accounts/profile/skill/$', add_skills, name='add_skill'),
    url(r'^accounts/portfolio/skills/delete/(?P<pk>\d+)/$', delete_skill, name='delete_skill'),
    url(r'^accounts/portfolio/skills/(?P<pk>\d+)/$', SkillUpdate.as_view(), name='update_skill'),


    url(r'^accounts/profile/experience/$', add_experience, name='add_experience'),
    url(r'^accounts/portfolio/experience/delete/(?P<pk>\d+)/$', delete_experience, name='delete_experience'),
    url(r'^accounts/portfolio/experience/(?P<pk>\d+)/$', ExperienceUpdate.as_view(), name='update_experience'),



    url(r'^accounts/profile/project/$', add_projects, name='add_projects'),
    url(r'^accounts/portfolio/project/delete/(?P<pk>\d+)/$', delete_project, name='delete_project'),
    url(r'^accounts/portfolio/project/(?P<pk>\d+)/$', ProjectUpdate.as_view(), name='update_project'),

    url(r'^accounts/portfolio/my_applications/$', my_applications, name='my_applications'),


    url(r'^accounts/employer/$', employer_portfolio, name="employer_portfolio"),
    url(r'^accounts/employer/delete_job/(?P<pk>\d+)/$', DeleteJobs.as_view(), name="job_delete"),
    url(r'^accounts/employer/job_details/(?P<pk>\d+)/$', job_details, name="job_details"),

    url(r'^accounts/employers_list/$', employers_list, name='employers_list'),
    url(r'^accounts/employer_details/(?P<pk>\d+)/$', employer_details, name="employer_details"),
    url(r'^accounts/employer_details/job_application/(?P<pk>\d+)/$', job_application, name="job_application"),


    url(r'^accounts/candidates_list/(?P<pk>\d+)/$', candidates_list, name="candidates_list"),
    url(r'^accounts/candidate_details/(?P<pk>\d+)/$', candidate_details, name="candidate_details"),

]
