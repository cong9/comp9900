from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import *

class LoginForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('username', 'password')


class UserCreationForm(UserCreationForm):
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)
    email = forms.EmailField(required=True)
    class Meta:
        model = User
        fields = ("first_name", "last_name", "username", "email", "password1", "password2")

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        user.first_name = self.cleaned_data["first_name"]
        user.last_name = self.cleaned_data["last_name"]
        if commit:
            user.save()
        return user

class SummaryForm(forms.ModelForm):
    class Meta:
        model = Summary
        fields = ('summary',)

        widgets = {
            'summary': forms.Textarea(attrs={'class':'form-control', 'rows':'4', 'style':'max-width: 70%;', 'placeholder':'Type summary here'})
        }


class EducationForm(forms.ModelForm):
    class Meta:
        model = Education
        fields = ('university', 'course', 'gpa', 'description', 'date_start', 'date_end')

        widgets = {
            'university':forms.TextInput(attrs={'class': 'form-control'}),
            'course':forms.TextInput(attrs={'class': 'form-control'}),
            'gpa':forms.NumberInput(attrs={'class': 'form-control'}),
            'description':forms.Textarea(attrs={'class': 'form-control', 'rows':'5','placeholder':'Type description here'}),
            'date_start':forms.TextInput(attrs={'id':'datepicker', 'class': 'form-control'}),
            'date_end':forms.TextInput(attrs={'id':'datepicker2', 'class': 'form-control', 'placeholder': "Leave empty if still studying."})
        }




class SkillsForm(forms.ModelForm):

    class Meta:
        model = Skill
        fields = ('name', 'description')
        widgets = {
            #'name':forms.ChoiceWidget(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'rows':'5', 'placeholder':'Type description here'})
        }


class ExperienceForm(forms.ModelForm):
    class Meta:
        model = Experience
        fields = ('organization', 'role', 'date_start', 'description', 'date_end')
        widgets ={
            'organization':forms.TextInput(attrs={'class': 'form-control'}),
            'role':forms.TextInput(attrs={'class': 'form-control'}),
            'description':forms.Textarea(attrs={'class': 'form-control', 'rows':'5', 'placeholder':'Type description here'}),
            'date_start':forms.TextInput(attrs={'id':'datepicker', 'class': 'form-control', 'placeholder':'mm/dd/yyyy'}),
            'date_end':forms.TextInput(attrs={'id':'datepicker2', 'class': 'form-control', 'placeholder':'mm/dd/yyyy'})

        }


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ('name', 'description', 'date_start', 'date_grad')
        widgets = {
                'name': forms.TextInput(attrs={'class':'form-control'}),
                'description': forms.Textarea(attrs={'class':'form-control'}),
                'date_start': forms.TextInput(attrs={'id':'datepicker', 'class': 'form-control', 'placeholder':'mm/dd/yyyy'}),
                'date_grad':forms.TextInput(attrs={'id':'datepicker2', 'class': 'form-control', 'placeholder':'mm/dd/yyyy'})
        }


class EmployerDescriptionForm(forms.ModelForm):
    class Meta:
        model = Employer
        fields = ("description", )
        widgets ={
            'description':forms.Textarea(attrs={'class': 'form-control'}),
        }

class JobsForm(forms.ModelForm):
    class Meta:
        model = Jobs
        fields = ("job_title", "job_description", "date_expiry")
        widgets = {
                "job_title": forms.TextInput(attrs={'class': 'form-control'}),
                "job_description": forms.Textarea(attrs={'rows':3, 'class': 'form-control'}),
                "date_expiry": forms.TextInput(attrs={'id':'datepicker', 'class':'form-control'})
        }
