from django.db import models
from django.contrib.auth.models import User
from user_management.models import Employer
from django.utils import timezone
from .skills_list import skill_list


# Defines the various database tables for the project.
# Please refer to the report for a detailed description of the database schema.


class Summary(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    summary = models.TextField(blank=True, null=True)

    def __str__(self):
        return str(self.summary)



class Education(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=None)
    university = models.CharField(max_length=100, default=None)
    course = models.CharField(max_length=150, default=None)
    gpa = models.DecimalField(null=True, max_digits=4, decimal_places=2)
    description = models.TextField(default=None)
    date_start = models.DateField(default=None)
    date_end = models.DateField(null=True, blank=True)

    class Meta:
        verbose_name_plural = "Education"
        unique_together = ('user', 'university', 'course')

    def __str__(self):
        return str(self.university)


class Skill(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    choices = [(str(x),y) for x, y in enumerate(skill_list)]
    name = models.CharField(max_length=3, choices=choices)
    description = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name_plural = "Skill"
        unique_together = ('user', 'name')

    def __str__(self):
        return str(self.name)


class Project(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)
    date_start = models.DateField()
    date_grad = models.DateField(null=True)

    class Meta:
        verbose_name_plural = "Project"
        unique_together = ('user', 'name')

    def __str__(self):
        return str(self.name)


class Experience(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    organization = models.CharField(max_length=50)
    role = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)
    date_start = models.DateField(default=None)
    date_end = models.DateField(null=True)

    class Meta:
        verbose_name_plural = "Experience"
        unique_together = ('user', 'organization', 'role')

    def __str__(self):
        return str(self.organization) + " " + str(self.role)



class Jobs(models.Model):
    job_poster = models.ForeignKey(Employer, on_delete=models.CASCADE)
    job_title = models.CharField(max_length = 255, default=None)
    date_posted = models.DateField(default=timezone.now, blank=True, null=True)
    date_expiry = models.DateField(default=None)
    job_description = models.TextField()

    class Meta:
        verbose_name_plural = "Jobs"

    def __str__(self):
        return str(self.job_title)

    def get_absolute_url(self):
        return reverse('portfolio:employer_portfolio')


class SkillsRequired(models.Model):
    job = models.ForeignKey(Jobs, on_delete=models.CASCADE)
    # skills_choices = [(str(x),y) for x, y in enumerate(skill_list)]
    skill = models.CharField(max_length=50)

    class Meta:
        verbose_name_plural = "Skills Required"
        unique_together = ("job", "skill")


class Job_Applications(models.Model):
    candidate = models.ForeignKey(User, on_delete=models.CASCADE)
    job = models.ForeignKey(Jobs, on_delete=models.CASCADE)
    date_applied = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Job Applications'
        unique_together = ('candidate', 'job')
