from django.contrib import admin
from .models import *
# Register your models here.
@admin.register(Summary)
class SummaryAdmin(admin.ModelAdmin):
    list_display = ['user', "summary"]


@admin.register(Education)
class EducationAdmin(admin.ModelAdmin):
    list_display = ['user', 'university', 'course', 'gpa', 'description', 'date_start', 'date_end']

@admin.register(Skill)
class SkillAdmin(admin.ModelAdmin):
    list_display = ['user', 'name', 'description']

@admin.register(Experience)
class ExperienceAdmin(admin.ModelAdmin):
    list_display = ['user', 'organization', 'role']

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ['user', 'name']

@admin.register(Jobs)
class JobsAdmin(admin.ModelAdmin):
    list_display = ["job_poster", "job_description"]

@admin.register(SkillsRequired)
class SkillsRequiredAdmin(admin.ModelAdmin):
    list_display = ["job", "skill"]

admin.site.register(Job_Applications)
