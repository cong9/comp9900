from django.shortcuts import redirect, HttpResponse, HttpResponseRedirect, reverse
from rating.apps.apps import get_search, update_skill_rating
from django.core.mail import send_mail, EmailMultiAlternatives
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import UpdateView, DeleteView
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from django.template.loader import get_template
from user_management.models import Employer
from django.template.defaulttags import register
from django.urls import reverse_lazy
from .skills_list import skill_list
from django.shortcuts import render
from django.contrib import messages
from django.conf import settings
from .forms import *
from .models import *
import json

# Create your views here.
def sendEmail(subject, content, recipient_list, text_content='', from_email=settings.EMAIL_HOST_USER):
    """
        Function to send emails to a list of recipients.
    """
    msg = EmailMultiAlternatives(subject, text_content, from_email, recipient_list)
    msg.attach_alternative(content, "text/html")
    msg.send()
    return None


#Render Homepage view.
def index(request):
    template = "portfolio/home.html"
    user = request.user
    users = User.objects.all()
    for usr in users:
        update_skill_rating(usr)
    is_emp=False
    if not user.is_anonymous:
        is_emp = Employer.objects.filter(username=user).exists()

    return render(request, template, {"is_emp": is_emp})

#Renders the user portfolio
@login_required # decorator to restrict access for anonymous users
def user_portfolio(request):
    template = 'portfolio/index2.html'

    if request.method == "GET":
        user = request.user
        summary = Summary.objects.filter(user=user)
        summary_form = None
        if len(summary)==0:
            summary_form = SummaryForm()

        educations = Education.objects.filter(user=user)
        skills = Skill.objects.filter(user=user)
        experieces = Experience.objects.filter(user=user)
        projects = Project.objects.filter(user=user)
        return render(request, template, context={'portfolio':user, 'educations':educations, 'skills':skills, 'experiences':experieces, 'summary':summary, 'projects':projects,
        'summary_form': summary_form})

    # Edit summary. Displayed on the portoflio page to update summary section.
    if request.method == "POST":
        form = SummaryForm(request.POST)
        user = request.user
        if form.is_valid():
            summary = form.cleaned_data.get('summary')
            user_summary = Summary.objects.create(user=user, summary=summary)

            return redirect('portfolio:account_redirect')

    return HttpResponseRedirect(reverse("portfolio:account_redirect"))

# View to delete summary.
@login_required
def delete_summary(request, pk):
    summary = Summary.objects.filter(id=pk).delete()
    return redirect('portfolio:account_redirect')


# Adds new education field for a user
# update_skill_rating function is called whenever education, skills, projects and experience
# database instances are created or updated.
# This allows the recalculation of portfolio rating for individual skills.
@login_required
def add_education(request):

    template = 'portfolio/add_education.html'
    if request.method == "GET":
        user = request.user
        form = EducationForm()
        return render(request, template, {'user': user, 'form':form})

    else:
        form = EducationForm(request.POST)
        print(form)
        user = request.user
        if form.is_valid():
            university = form.cleaned_data.get('university')
            course = form.cleaned_data.get('course')
            gpa = form.cleaned_data.get('gpa')
            description = form.cleaned_data.get('description')
            date_start = form.cleaned_data.get('date_start')
            date_end = form.cleaned_data.get('date_end')
            education = Education.objects.create(user=user, university=university,course=course, gpa=gpa, description=description, date_start=date_start, date_end =date_end)
            update_skill_rating(request.user)
            return redirect('portfolio:account_redirect')
        return render(request, template, {'user': user, 'form':form})

# View to delete individual education
def delete_education(request, pk):
    education = Education.objects.filter(id=pk).delete()
    update_skill_rating(request.user)
    return redirect('portfolio:account_redirect')

# ClassBased View to update Education section.
# ClassBased Generic UpdateView makes it easy to update a database model instance.
# LoginRequiredMixin is used to restrict access for users who are not logged in.
class EducationUpdate(LoginRequiredMixin, UpdateView):
    login_url = 'login'
    model = Education
    context_object_name = 'education'
    form_class = EducationForm
    template_name = 'portfolio/update_education.html'
    success_url = reverse_lazy('portfolio:account_redirect')
    def post(self, request, **kwargs):
        update_skill_rating(request.user)
        return super(EducationUpdate, self).post(request, **kwargs)

# Add new skills
@login_required
def add_skills(request):
    template = template = 'portfolio/add_skill.html'

    if request.method == "GET":
        user = request.user
        form = SkillsForm()
        return render(request, template, {'form':form})

    else:
        form = SkillsForm(request.POST)
        user = request.user
        if form.is_valid():
            name = form.cleaned_data.get('name')
            description = form.cleaned_data.get('description')
            skill = Skill.objects.create(user=user, name=name, description=description)

            update_skill_rating(user)   # update the skill rating for the user whenever a new skill is added
            return redirect('portfolio:account_redirect')
    return render(request, template, {'form':form})

#Delete a skill
@login_required
def delete_skill(request, pk):
    skill = Skill.objects.get(id=pk).delete()
    # update skill ratings when a skill is deleted
    update_skill_rating(request.user)
    return redirect('portfolio:account_redirect')

# Update an existing skill
class SkillUpdate(LoginRequiredMixin, UpdateView):
    login_url = 'login'
    model = Skill
    context_object_name = 'skill'
    form_class = SkillsForm
    template_name = 'portfolio/update_skill.html'
    success_url = reverse_lazy('portfolio:account_redirect')

    # update the skill rating whenever a skill is updated
    def post(self, request, **kwargs):
        update_skill_rating(request.user)
        return super(SkillUpdate, self).post(request, **kwargs)

# Add experience view
@login_required
def add_experience(request):
    template = 'portfolio/add_experience.html'

    if request.method == "GET":
        user = request.user
        form = ExperienceForm()
        return render(request, template, {'form':form})

    else:
        form = ExperienceForm(request.POST)
        user = request.user
        if form.is_valid():
            organization = form.cleaned_data.get('organization')
            role = form.cleaned_data.get('role')
            description = form.cleaned_data.get('description')
            date_start = form.cleaned_data.get('date_start')
            date_end = form.cleaned_data.get('date_end')
            experiece = Experience.objects.create(user=user, organization=organization, description=description, role=role, date_start=date_start, date_end=date_end)

            # update skill rating when a new experience is added
            update_skill_rating(request.user)
            return redirect('portfolio:account_redirect')
    return render(request, template, {'form': form})

#delete experience instance and update skill ratings
def delete_experience(request, pk):
    experience = Experience.objects.get(id=pk).delete()
    update_skill_rating(request.user)
    return redirect('portfolio:account_redirect')

# Update experience and respective skill ratings
class ExperienceUpdate(LoginRequiredMixin, UpdateView):
    login_url = 'login'
    model = Experience
    context_object_name = 'experience'
    form_class = ExperienceForm
    template_name = 'portfolio/update_experience.html'
    success_url = reverse_lazy('portfolio:account_redirect')

    def post(self, request, **kwargs):
        update_skill_rating(request.user)
        return super(ExperienceUpdate, self).post(request, **kwargs)


# Add new projects
@login_required
def add_projects(request):
    template = 'portfolio/add_projects.html'
    if request.method == "GET":
        user = request.user
        form = ProjectForm()
        return render(request, template, {'form':form})

    else:
        form = ProjectForm(request.POST)
        user = request.user
        if form.is_valid():
            name = form.cleaned_data.get('name')
            description = form.cleaned_data.get('description')
            date_start = form.cleaned_data.get('date_start')
            date_grad = form.cleaned_data.get('date_grad')
            project = Project.objects.create(user=user, name=name, description=description, date_start=date_start, date_grad=date_grad)
            update_skill_rating(request.user)
            return redirect('portfolio:account_redirect')

# delete projects
@login_required
def delete_project(request, pk):
    project = Project.objects.get(id=pk).delete()
    update_skill_rating(request.user)
    return redirect('portfolio:account_redirect')

#Update existing projects and skill ratings accordingly
class ProjectUpdate(LoginRequiredMixin, UpdateView):
    login_url = 'login'
    model = Project
    context_object_name = 'project'
    form_class = ProjectForm
    template_name = 'portfolio/update_project.html'
    success_url = reverse_lazy('portfolio:account_redirect')

    def post(self, request, **kwargs):
        update_skill_rating(request.user)
        return super(ProjectUpdate, self).post(request, **kwargs)

# list view to list job applications siubmitted by the user
@login_required
def my_applications(request):
    user = request.user
    applications = Job_Applications.objects.filter(candidate=user)
    template = 'portfolio/my_applications.html'
    return render(request, template, {'my_applications':applications})

# Renders the portfolio of an employer. Only accissable by employer users
@login_required
def employer_portfolio(request):
    if request.method == 'GET':
        user = request.user
        employer = Employer.objects.get(username=user)
        form = EmployerDescriptionForm(initial={"description":employer.description})
        jobs_form = JobsForm()
        jobs = Jobs.objects.filter(job_poster=employer)
        skills = skill_list
        return render(request, 'portfolio/employer_portfolio.html', {'employer':employer, "form":form, "jobs_form":jobs_form, "jobs": jobs, "len_skills":len(skills), 'skills':skills})


    # Since a single page allows updating description and creating job postings,
    # a post request is catogorised as 'description' or 'job'
    if request.method == "POST":
        # For a description update
        if request.POST.get('submit') == "description":
            form = EmployerDescriptionForm(request.POST)
            user = request.user
            if form.is_valid():
                description = form.cleaned_data.get("description")
                employer = Employer.objects.filter(username=user).update(description=description)
                return redirect("portfolio:employer_portfolio")


        # for a job posting update
        if request.POST.get("submit") == "job":
            form = JobsForm(request.POST)
            user = request.user
            employer = Employer.objects.get(username=user)
            skills_dict = dict(request.POST)
            skills = skills_dict.get('skills')
            if form.is_valid():
                job_title = form.cleaned_data.get("job_title")
                job_description = form.cleaned_data.get("job_description")
                date_expiry = form.cleaned_data.get("date_expiry")

                job = Jobs.objects.create(job_poster=employer, job_title=job_title, job_description=job_description, date_expiry=date_expiry)

                if skills:
                    for skill in skills:
                        skill_obj = SkillsRequired.objects.create(job=job, skill=skill)
                return redirect("portfolio:employer_portfolio")

# Delete jobs
class DeleteJobs(LoginRequiredMixin, DeleteView):
    login_url = 'login'
    model = Jobs
    template_name = 'portfolio/employer_portfolio.html'
    success_url = reverse_lazy('portfolio:employer_portfolio')

# Details View for job postings
# allows students to view and apply for jobs
@login_required
def job_details(request, pk):
    if request.method == "GET":
        job = Jobs.objects.get(id=pk)
        job_form = JobsForm(initial={'job_title': job.job_title, 'job_description':job.job_description,
                    'date_expiry':job.date_expiry})
        template = 'portfolio/job_details.html'
        return render(request, template, {'job':job, 'job_form':job_form})

    elif request.method=="POST":
        form = JobsForm(request.POST)
        user = request.user
        employer = Employer.objects.get(username=user)
        if form.is_valid():
            job_title = form.cleaned_data.get("job_title")
            job_description = form.cleaned_data.get("job_description")
            date_expiry = form.cleaned_data.get("date_expiry")
            job = Jobs.objects.filter(id=pk).update(job_title=job_title, job_description=job_description, date_expiry=date_expiry)

            return redirect("portfolio:employer_portfolio")

# display list of employers to students
@login_required
def employers_list(request):
    template = 'portfolio/employers_list.html'
    user = request.user
    employers = Employer.objects.all()
    return render(request, template, {'employers':employers, 'user':user})

# View the details of an employer together with the details of the jobs posted.
@login_required
def employer_details(request, pk):
    @register.filter
    def get_item(dictionary, key):
        return dictionary.get(key)
    employer = Employer.objects.get(id=pk)
    jobs = Jobs.objects.filter(job_poster=employer)
    mapping = {}
    for j in jobs:
        if j not in mapping:
            mapping[j.job_title] = [x.skill for x in SkillsRequired.objects.filter(job=j)]
    print(mapping)
    template = "portfolio/employer_details.html"
    return render(request, template, {"employer":employer, 'jobs':jobs, 'map':mapping})

# Apply for jobs, triggers a confirmation email to be sent to the student's email address
@login_required
def job_application(request, pk):
    user = request.user
    if request.method == "POST":
        job = Jobs.objects.get(id=pk)
        if not Job_Applications.objects.filter(candidate=user, job=job).exists():
            application = Job_Applications.objects.create(candidate=user, job=job)
            messages.success(request, "Your application is on the way.")
            data = {'first_name':user.first_name, 'job':job.job_title, 'employer':job.job_poster.username.first_name}
            template = get_template('emails/job_application.html')
            subject = 'Your application is submitted [DO NOT REPLY].'
            content = template.render(data)
            recipient_list = [user.email]
            sendEmail(subject=subject, content=content, recipient_list=recipient_list)
            return redirect('portfolio:employers_list')
        else:
            messages.error(request, "You have already applied for this job.")
            return redirect('portfolio:employers_list')

# Generates the list of suitable candidates for job by calculating an average of
# skill ratings matching with the job.
# calculates the student portfolio rating based on the job requorements and displays portfolios
# according to a non-increasing order of ratings.
@login_required
def candidates_list(request, pk):
    template = 'portfolio/candidates_list.html'
    @register.filter
    def get_item(dictionary, key):
        return dictionary.get(key)

    if request.method == "GET":
        job = Jobs.objects.get(id=pk)

        job_skills = SkillsRequired.objects.filter(job=job)
        job_applications = Job_Applications.objects.filter(job=job)
        user_skills = {}
        for skill in job_skills:

            user_skill_rating_obj = get_search(skill.skill)
            for user, rating in user_skill_rating_obj.items():
                if user_skills.get(user) is None:
                    user_skills[user] = {'rating': 0, 'count': 0}
                user_skills[user]['count'] += 1
                user_skills[user]['rating'] += rating
                #print(user_skills)

        user_ratings = {user: rating['rating']/rating['count'] for user, rating in user_skills.items()}  # average score of all skills
        users = user_ratings.keys()

        candidates = sorted([{'user': user, 'rating': user_ratings[user.id]} for user in User.objects.filter(id__in=users)],
                            key=lambda x: -x['rating'])  # sorted scores
        if len(candidates) > 0:
            max_score = max(candidates, key=lambda x: x['rating'])['rating']
            min_score = min(candidates, key=lambda x: x['rating'])['rating']
            for can in candidates:
                can['rating'] = round((can['rating'] - min_score)/(max_score-min_score) * 10, 1)  # normalise scores
        to_remove = []

        return render(request, template, {'candidates':candidates, 'applications':job_applications})


@login_required
def candidate_details(request, pk):
    template = "portfolio/candidate_details.html"
    candidate = User.objects.get(id=pk)
    summary = Summary.objects.filter(user=candidate)
    education = Education.objects.filter(user=candidate)
    skills = Skill.objects.filter(user=candidate)
    experience = Experience.objects.filter(user=candidate)
    projects = Project.objects.filter(user=candidate)

    return render(request, template, {"candidate":candidate, "summary":summary, "educations":education,
                                        "skills":skills, "experiences":experience, "projects":projects})
