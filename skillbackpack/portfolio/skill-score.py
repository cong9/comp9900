#!/usr/bin/env python3
# Author: Hai C Nguyen
# Created: 180903-1906


import numpy as np
def way_2(wordlist1,wordlist2):
    # reference from "string - python: How to calculate the cosine similarity of two word lists?". <https://stackoverflow.com/questions/28819272/python-how-to-calculate-the-cosine-similarity-of-two-word-lists> viewed 180903-1934
    #
    # This is faster than the manual-calculation method way_1() below

    wordlist1 = [ a.lower() for a in wordlist1 ]
    wordlist2 = [ a.lower() for a in wordlist2 ]

    vocab = {}
    i = 0

    for word in wordlist1:
        if word not in vocab:
            vocab[word] = i
            i += 1


    for word in wordlist2:
        if word not in vocab:
            vocab[word] = i
            i += 1

    # create a numpy array (vector) for each input, filled with zeros
    wordlist1_np = np.zeros(len(vocab))
    wordlist2_np = np.zeros(len(vocab))

    # loop through each input and create a corresponding vector for it
    # this vector counts occurrences of each word in the dictionary

    for word in wordlist1:
        index = vocab[word] # get index from dictionary
        wordlist1_np[index] += 1 # increment count for that index

    for word in wordlist2:
        index = vocab[word]
        wordlist2_np[index] += 1

    # The final step is actual calculation of the cosine similarity.

    # use numpy's dot product to calculate the cosine similarity
    sim = np.dot(wordlist1_np, wordlist2_np) / np.sqrt(np.dot(wordlist1_np, wordlist1_np) * np.dot(wordlist2_np, wordlist2_np))

    return sim


from collections import Counter
def way_1(wordlist1, wordlist2):  # slower than the numpy method

    wordlist1 = [ a.lower() for a in wordlist1 ]
    wordlist2 = [ a.lower() for a in wordlist2 ]

    # count word occurrences
    wordlist1_vals = Counter(wordlist1)
    wordlist2_vals = Counter(wordlist2)

    # convert to word-vectors
    words  = list(wordlist1_vals.keys() | wordlist2_vals.keys())
    wordlist1_vect = [wordlist1_vals.get(word, 0) for word in words]        # [0, 0, 1, 1, 2, 1]
    wordlist2_vect = [wordlist2_vals.get(word, 0) for word in words]        # [1, 1, 1, 0, 1, 0]

    # find cosine
    len_wordlist1  = sum(av*av for av in wordlist1_vect) ** 0.5             # sqrt(7)
    len_wordlist2  = sum(wordlist2v*wordlist2v for wordlist2v in wordlist2_vect) ** 0.5             # sqrt(4)
    dot    = sum(av*bv for av,bv in zip(wordlist1_vect, wordlist2_vect))    # 3
    cosine = dot / (len_wordlist1 * len_wordlist2)                          # 0.5669467

    return cosine


import gensim
def way_3(wordlist1,wordlist2, wordlist1_superset, wordlist2_superset):
    """calculate the cosine similarity score between wordlist1 and wordlist2, all words in wordlist1 must have appeared in wordlist1_superset and similarly for wordlist2"""

    wordlist1 = [ a.lower() for a in wordlist1 ]
    wordlist2 = [ a.lower() for a in wordlist2 ]
    wordlist1_superset = [ a.lower() for a in wordlist1_superset ]
    wordlist2_superset = [ a.lower() for a in wordlist2_superset ]


    sentences = [wordlist1_superset, wordlist2_superset]

    model = gensim.models.Word2Vec(sentences, min_count=1)

    return (model.n_similarity(wordlist1, wordlist2))


if __name__ == "__main__":
    # word-lists to compare
    wordlist1 = [u'C++', u'python', u'shell']
    wordlist2 = [u'shell', u'python', u'C++', u'perl']


    print("similarity between {} and {}:", wordlist1, wordlist2)

    import time
    t1 = time.clock()
    result = way_1(wordlist1,wordlist2)
    t2=time.clock() - t1
    print("method 1 (manual calculation): {} (finished in {} sec)".format(result,t2))

    t1 = time.clock()
    result = way_2(wordlist1,wordlist2)
    t2=time.clock() - t1
    print("method 2 (using numpy): {} (finished in {} sec)".format(result,t2))

    t1 = time.clock()
    result = way_3(wordlist1,wordlist2, wordlist1, wordlist2)
    t2=time.clock() - t1
    print("method 3 (using gensim): {} (finished in {} sec)".format(result,t2))
