from django.db import models
from django.contrib.auth.models import User
from collections import defaultdict
from django.db.models.signals import post_save

# Create your models here.


class Employer(models.Model):
    username = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.TextField(blank=True, null=True, default="Description")

    class Meta:
        verbose_name_plural = "Employers"


    def __str__(self):
        return str(self.username.first_name)
