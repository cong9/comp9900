from django.shortcuts import redirect, HttpResponse, HttpResponseRedirect, reverse
from django.core.mail import send_mail, EmailMultiAlternatives
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.conf import settings
from django.contrib.auth.models import User
from django.template.loader import get_template
from django.shortcuts import render
from django.contrib import messages
from portfolio.models import *
from .forms import *



def sendEmail(subject, content, recipient_list, text_content='', from_email=settings.EMAIL_HOST_USER):
    msg = EmailMultiAlternatives(subject, text_content, from_email, recipient_list)
    msg.attach_alternative(content, "text/html")
    msg.send()
    return None


# Create your views here.
# signin and sign_up view for students
def signin(request):
    if request.method == 'POST':
        if request.POST.get('submit') == 'sign_in':
            form = LoginForm(request.POST)
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(username=username, password=password)
            if user is None:
                print("Login Failed.")
                messages.error(request,"Login failed. Invalid credentials.")
                #print(messages.error(request,"User does not exists."))
                return redirect('login')
            else:
                login(request, user)
                return HttpResponseRedirect(reverse('portfolio:account_redirect'))

        if request.POST.get('submit') == 'sign_up':
            form = StudentUserCreationForm(request.POST)
            if form.is_valid():
                form.save()
                username = form.cleaned_data.get('username')
                raw_password = form.cleaned_data.get('password1')
                user = authenticate(username=username, password=raw_password)

                user = User.objects.get(username=username)

                login(request, user)

                data = {'first_name':user.first_name}
                template = get_template('emails/welcome.html')
                subject = 'Welcome to Skillbackpack [DO NOT REPLY].'
                content = template.render(data)
                recipient_list = [user.email]
                sendEmail(subject=subject, content=content, recipient_list=recipient_list)
                return HttpResponseRedirect(reverse('portfolio:account_redirect'))
            else:
                messages.error(request,form.errors)
                return redirect('login')

    signup_form = StudentUserCreationForm()
    signin_form = LoginForm()

    return render(request, 'registration/login.html', {'form_signup': signup_form, 'form_login': signin_form})

# signin and signup view for the employers
def signin_employer(request):
    if request.method == 'POST':

        if request.POST.get('submit') == 'sign_in':
            print("abc1")
            form = LoginForm(request.POST)
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(username=username, password=password)
            print(user)
            if user is None:
                messages.error(request,form.errors)
                return redirect('user_management:employer_login')
            else:
                login(request, user)
                return HttpResponseRedirect(reverse('portfolio:employer_portfolio'))

        if request.POST.get('submit') == 'sign_up':
            print("Comes here")
            form = EmployerCreationForm(request.POST)

            if form.is_valid():
                form.save()
                username = form.cleaned_data.get('username')
                raw_password = form.cleaned_data.get('password1')
                user = authenticate(username=username, password=raw_password)
                user = User.objects.get(username=username)
                employer = Employer.objects.get_or_create(username=user)

                login(request, user)

                data = {'first_name':user.first_name}
                template = get_template('emails/welcome.html')
                subject = 'Welcome to Skillbackpack [DO NOT REPLY].'
                content = template.render(data)
                recipient_list = [user.email]
                sendEmail(subject=subject, content=content, recipient_list=recipient_list)
                return HttpResponseRedirect(reverse('portfolio:employer_portfolio'))
            else:
                messages.error(request,form.errors)
                return redirect('user_management:employer_login')

    signup_form = EmployerCreationForm()
    signin_form = LoginForm()

    return render(request, 'registration/employer_login.html', {'form_signup': signup_form, 'form_login': signin_form})
