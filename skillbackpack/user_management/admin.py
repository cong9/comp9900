from django.contrib import admin
from .models import *

@admin.register(Employer)
class EmployerAdmin(admin.ModelAdmin):
    list_display = ['username',]
# Register your models here.
